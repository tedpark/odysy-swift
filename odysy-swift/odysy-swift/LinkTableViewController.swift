//
//  LinkTableViewController.swift
//  odysy-swift
//
//  Created by ted on 1/15/17.
//  Copyright © 2017 Ted Park. All rights reserved.
//

import UIKit
import Kanna
import SwiftSoup
import SwiftyJSON
import GoogleMobileAds
import NVActivityIndicatorView

let appid : String = "ca-app-pub-8354514655539274/3172381224"

class LinkTableViewController: UITableViewController, GADBannerViewDelegate, NVActivityIndicatorViewable {

//	Array(Set(posts)
	var adBannerView: GADBannerView!
    var urlArray = [String]()
	var parseUrlArray = [String]()
	var cellArray = [Array<Any>]()
	var num = 0
	var passUrl = String()
    var download_requests = [URLSessionDataTask]()

    override func viewDidLoad() {
        super.viewDidLoad()

		NotificationCenter.default.addObserver(self, selector: #selector(self.startLoading(notification:)), name: Notification.Name("StartNotificationIdentifier"), object: nil)

		//LinkTableViewCell
		self.tableView.register(UINib(nibName: "LinkTableViewCell", bundle: nil), forCellReuseIdentifier: "LinkTableViewCell")

		//NoImgLinkTableViewCell
		self.tableView.register(UINib(nibName: "NoImgLinkTableViewCell", bundle: nil), forCellReuseIdentifier: "NoImgLinkTableViewCell")

		//AdNewTableViewCell
		self.tableView.register(UINib(nibName: "AdNewTableViewCell", bundle: nil), forCellReuseIdentifier: "AdNewTableViewCell")

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LinkTableViewController.close))
        self.navigationItem.rightBarButtonItem?.tintColor = .black

		adBannerView = GADBannerView(adSize: kGADAdSizeMediumRectangle)
		adBannerView?.adUnitID = appid

		adBannerView.rootViewController = self
		adBannerView.load(GADRequest())

		adBannerView?.delegate = self
		adBannerView?.backgroundColor = UIColor.red
//		        adBannerView?.rootViewController = self
		adBannerView.load(GADRequest())

    }



    @objc func startLoading(notification: Notification) {
		print(notification.userInfo!)
		if let paramUrl = notification.userInfo?["url"] as? String {
			let paramUrl = NSURL(string: paramUrl)
			self.linkParsing(paramUrl: paramUrl! as URL)
		}
	}



	func linkParsing(paramUrl: URL) {

		parseUrlArray.removeAll()

		var urlString = "http://ec2-54-88-114-129.compute-1.amazonaws.com:3412/api?originUrl=\(paramUrl)"
		urlString = urlString.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
		let url = URL(string: urlString)

        if url == nil {
            return
        }
		var request = URLRequest(url: url!)
		request.httpMethod = "GET"
//		self.showWaitOverlay()
		let size = CGSize(width: 30, height: 30)
        let indicatorType = NVActivityIndicatorType.ballPulseSync
        startAnimating(size, message: "Loading...", type: indicatorType, fadeInAnimation: nil)
        
        
        
//		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
//			NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
//		}
//		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
//			self.stopAnimating()
//		}

		let task = URLSession.shared.dataTask(with: request) { data, response, error in
			guard error == nil else {
				print(error!)
//				self.removeAllOverlays()
				DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
					self.stopAnimating()
				}
				return
			}
			guard let data = data else {
				print("Data is empty")
//				self.removeAllOverlays()
				DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
					self.stopAnimating()
				}
				return
			}

			let json = try! JSON(data: data)

			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.4) {
				self.stopAnimating()
			}
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
				NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
			}
//			self.removeAllOverlays()
//			print(json)

			let paramUrlEncode = URL(string: paramUrl.absoluteString)
			for (_, subJson):(String, JSON) in json["parseLink"] {
				self.parseUrlArray.append(subJson.string!)
				DispatchQueue.main.async {
					self.getPreview(urlParam: subJson.string!, urlOrigin: paramUrlEncode!)
				}

//				print(subJson)
			}
		}

		task.resume()

	}



    @objc func close() {
        print("test")
        self.dismiss(animated: true)
    }



	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 250
	}






	func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
		let size = image.size

		let widthRatio  = targetSize.width  / image.size.width
		let heightRatio = targetSize.height / image.size.height

		// Figure out what our orientation is, and use that to form the rectangle
		var newSize: CGSize
		if(widthRatio > heightRatio) {
			newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
		} else {
			newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
		}

		// This is the rect that we've calculated out and this is what is actually used below
		let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

		// Actually do the resizing to the rect using the ImageContext stuff
		UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
		image.draw(in: rect)
		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()

		return newImage!
	}



	func getPreview(urlParam: String, urlOrigin: URL) {
		let parseUrl = "http://ec2-54-88-114-129.compute-1.amazonaws.com:9000/oembed?url="
		var url = URL(string: "")
		if urlParam.lowercased().range(of:"http") != nil || urlParam.lowercased().range(of:"https") != nil{
			url = URL(string: parseUrl + urlParam)
		} else {
			let originUrl = (urlOrigin.scheme)! + "://" + (urlOrigin.host)!
			url = URL(string: parseUrl + originUrl + urlParam)
		}

		guard url != nil else {
			return
		}

		url = URL(string: (url?.absoluteString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!)

		let task = URLSession.shared.dataTask(with: url!) { data, response, error in
			guard error == nil else {
				print(error!)
				return
			}
			guard let data = data else {
				print("Data is empty")
				return
			}

			let jsonTest = try! JSON(data: data)

			if let title = jsonTest["title"].string {
                if let thumbnail_url = jsonTest["thumbnail_url"].string {
					if thumbnail_url.lowercased().range(of:"http") != nil || thumbnail_url.lowercased().range(of:"https") != nil {
						if urlParam.lowercased().range(of:"http") != nil || thumbnail_url.lowercased().range(of:"https") != nil{
							url = URL(string: urlParam)
						} else {
							let originUrl = (urlOrigin.scheme)! + "://" + (urlOrigin.host)!
							url = URL(string: originUrl + urlParam)
						}
					} else {
						//#bug, 여기 풀면 한글이 문제가 생김
//						let originUrl = (urlOrigin.scheme)! + "://" + (urlOrigin.host)!
//						thumbnail_url = originUrl + thumbnail_url
						//#bug, 여기 풀면 한글이 문제가 생김
					}

					let data = try? Data(contentsOf: URL(string: thumbnail_url)!)

					if let imageData = data {
						let image = UIImage(data: imageData)

						if let description = jsonTest["description"].string {
							print(description)
							if urlParam.lowercased().range(of:"http") != nil || urlParam.lowercased().range(of:"https") != nil {
								print("exists")
								url = URL(string: urlParam)
							} else {
								print("none")
								let originUrl = (urlOrigin.scheme)! + "://" + (urlOrigin.host)!
								url = URL(string: originUrl + urlParam)
							}
							if (self.cellArray.count % 10 == 0 && self.cellArray.count != 0) {
								print(#line, self.cellArray.count);
//								self.cellArray.append(["ad", "ad", "ad", "ad"]);
							}
							self.cellArray.append([image!, title, description, url!])
						} else {
							if (self.cellArray.count % 10 == 0 && self.cellArray.count != 0) {
								print(#line, self.cellArray.count);
//								self.cellArray.append(["ad", "ad", "ad", "ad"]);
							}
							self.cellArray.append([image!, title, "description", url!])
						}
					}
					print(thumbnail_url)
				} else {
					let dummyText = "dummy"
					if let description = jsonTest["description"].string {
						if (self.cellArray.count % 10 == 0 && self.cellArray.count != 0) {
							print(#line, self.cellArray.count);
//								self.cellArray.append(["ad", "ad", "ad", "ad"]);
						}
						self.cellArray.append([dummyText, title, description, url!])
					} else {
                        //이미지 없고, 디스크립션 없으면 추가하지 않는다
//						self.cellArray.append([dummyText, title, "description", url!])
					}
				}
			}

			if error == nil {
				DispatchQueue.main.async() {
					self.tableView.reloadData()
					print("reload")
					print(self.cellArray.count)
				}
			}

		}
		DispatchQueue.main.async {
			self.download_requests.append(task)
			task.resume()
		}
	}


    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        //stop all download requests
        for request in download_requests
        {
            request.cancel()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.cellArray.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifierLinkCell", for: indexPath)

//		cell.urlAdress = self.urlArray[indexPath.row]
        // Configure the cell...
		if (self.cellArray[indexPath.row][0] is String && self.cellArray[indexPath.row][0] as! String != "ad") {
			let noImgLinkTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NoImgLinkTableViewCell", for: indexPath) as! NoImgLinkTableViewCell
			noImgLinkTableViewCell.titleLabel.text = (self.cellArray[indexPath.row][1] as! String)
			noImgLinkTableViewCell.contentsLabel.text = (self.cellArray[indexPath.row][2] as! String)
			return noImgLinkTableViewCell

		} else if (self.cellArray[indexPath.row][0] is String && self.cellArray[indexPath.row][1] as! String == "ad") {
			let adTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdNewTableViewCell", for: indexPath) as! AdNewTableViewCell
//			adTableViewCell.backgroundColor = UIColor.red
			adBannerView.center = CGPoint(x: UIScreen.main.bounds.size.width/2, y: adBannerView.frame.height/2)
			adTableViewCell.addSubview(adBannerView)
			return adTableViewCell

		} else {
			let linkTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LinkTableViewCell", for: indexPath) as! LinkTableViewCell
			linkTableViewCell.parseImage.image = (self.cellArray[indexPath.row][0] as! UIImage)
			linkTableViewCell.parseTitle.text = (self.cellArray[indexPath.row][1] as! String)
//			linkTableViewCell.parseContent.text = (self.cellArray[indexPath.row][2] as! String)
			return linkTableViewCell
		}
    }


//(self.cellArray[indexPath.row][0] is String && self.cellArray[indexPath.row][0] as! String != "ad") {
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//		let imageDataDict:[String: UIImage] = ["image": image]

		print("section: \(indexPath.section)")
		print("row: \(indexPath.row)")
		print(self.cellArray[indexPath.row][3])

		if self.cellArray[indexPath.row][0] is String && self.cellArray[indexPath.row][1] as! String == "ad" && self.cellArray[indexPath.row][2] as! String == "ad" {

		} else {
			let urlDataDict:[String: String] = ["url": (self.cellArray[indexPath.row][3] as! URL).absoluteString]
			self.dismiss(animated: true) {
				NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: urlDataDict)
			}
		}

	}

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}






//func parsing(url:URL) {
//    //        let url = URL(string: "https://httpbin.org/ip")
//    //        let url = URL(string: "http://ec2-54-88-114-129.compute-1.amazonaws.com:9000/oembed?url=")
//    let url = URL(string: "http://ec2-54-88-114-129.compute-1.amazonaws.com:9000/oembed?url=https://en.m.wikipedia.org/wiki/nintendo")
//    let task = URLSession.shared.dataTask(with: url!) { data, response, error in
//        guard error == nil else {
//            print(error!)
//            return
//        }
//        guard let data = data else {
//            print("Data is empty")
//            return
//        }
//        
//        let json = try! JSONSerialization.jsonObject(with: data, options: [])
//        print(json)
//    }
//    
//    task.resume()
//}
//
//func matches(for regex: String, in text: String) -> [String] {
//    
//    do {
//        let regex = try NSRegularExpression(pattern: regex)
//        let nsString = text as NSString
//        let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
//        return results.map { nsString.substring(with: $0.range)}
//    } catch let error {
//        print("invalid regex: \(error.localizedDescription)")
//        return []
//    }
//}
//
//
//func getHTML(url: URL) {
//    //        let url = URL(string: "https://httpbin.org/ip")
//    //        let url = URL(string: "http://ec2-54-88-114-129.compute-1.amazonaws.com:9000/oembed?url=")
//    
//    //        let url = URL(string: "https://medium.com")
//    
//    print((url.scheme)! + "://" + (url.host)!)
//    
//    let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
//        if let data = data,
//            let text = String(data: data, encoding: String.Encoding.utf8) {
//            //				print(html)
//            
//            let regex = try! NSRegularExpression(pattern: "<a[^>]+href=\"(.*?)\"[^>]*>")
//            let range = NSMakeRange(0, text.characters.count)
//            let matches = regex.matches(in: text, range: range)
//            for match in matches {
//                let htmlLessString = (text as NSString).substring(with: match.rangeAt(1))
//                print(htmlLessString)
//                self.num += 1
//                print(self.num)
//                
//                //					self.getPreview(urlParam: htmlLessString, urlOrigin: url!)
//                
//                if self.urlArray.contains(htmlLessString) {
//                    print("We've got!")
//                } else {
//                    print("No here – sorry!")
//                    self.urlArray.append(htmlLessString)
//                    self.getPreview(urlParam: htmlLessString, urlOrigin: url)
//                }
//            }
//            
//            if let doc = Kanna.HTML(html: text, encoding: String.Encoding.utf8) {
//                //                    print(doc.title)
//                // Search for nodes by XPath //"//a | //link"  //"/html/body//a/@href") {
//                //					for link in doc.xpath("/html/body//a") {
//                for link in doc.xpath("/html/body//a/@href") {
//                    //                    for link in doc.xpath("//a | //link") {
//                    print(#line, link.toHTML)
//                    /*
//                     print(link.toHTML)
//                     self.num += 1
//                     print(self.num)
//                     */
//                    //						if link["href"]?.range(of:"#") == nil{
//                    ////							print("not exists")
//                    //
//                    //							self.urlArray.append(link["href"]!)
//                    //						}
//                    
//                    //                        print(link["href"])
//                    
//                    //						self.getPreview(urlParam: link["href"]!, urlOrigin: url!)
//                    //						print(self.urlArray.count)
//                }
//            }
//            DispatchQueue.main.async() {
//                self.tableView.reloadData()
//                print("reload")
//            }
//            //                self.tableView.reloadData()
//        }
//        
//    }
//    task.resume()
//    
//    //        self.tableView.reloadData()
//    
//}
//
//func uniqueElementsFrom<T: Hashable>(array: [T]) -> [T] {
//    var set = Set<T>()
//    let result = array.filter {
//        guard !set.contains($0) else {
//            return false
//        }
//        set.insert($0)
//        return true
//    }
//    return result
//}
