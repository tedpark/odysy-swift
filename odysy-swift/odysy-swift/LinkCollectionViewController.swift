//
//  LinkCollectionViewController.swift
//  odysy-swift
//
//  Created by ted on 6/23/17.
//  Copyright © 2017 Ted Park. All rights reserved.
//

import UIKit
import Kanna
import SwiftSoup
import SwiftyJSON
import GoogleMobileAds
import NVActivityIndicatorView

private let reuseIdentifier = "LinkCollectionViewCell"

class LinkCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, NVActivityIndicatorViewable {
    
    var urlArray = [String]()
    var parseUrlArray = [String]()
    var cellArray = [Array<Any>]()
    var num = 0
    var passUrl = String()
    var download_requests = [URLSessionDataTask]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.close))

        NotificationCenter.default.addObserver(self, selector: #selector(self.startLoading(notification:)), name: Notification.Name("StartCollectionNotificationIdentifier"), object: nil)


        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
		self.collectionView!.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

		self.collectionView!.register(UINib(nibName: "NoImgLinkCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NoImgLinkCollectionViewCell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func startLoading(notification: Notification) {
        print(notification.userInfo!)
        if let paramUrl = notification.userInfo?["url"] as? String {
            let paramUrl = NSURL(string: paramUrl)
            //			self.getHTML(url: url! as URL)
            self.linkParsing(paramUrl: paramUrl! as URL)
            
        }
    }

    
    //http://ec2-54-88-114-129.compute-1.amazonaws.com:3412/api
    
    func linkParsing(paramUrl: URL) {
        
        parseUrlArray.removeAll()

        var urlString = "http://ec2-54-88-114-129.compute-1.amazonaws.com:3412/api?originUrl=\(paramUrl)"
        urlString = urlString.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        let url = URL(string: urlString)

        if url == nil {
            return
        }

//        let url = URL(string: "http://ec2-54-88-114-129.compute-1.amazonaws.com:3412/api?originUrl=\(paramUrl)")

//		url = URL(string: (url?.absoluteString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)!)

        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
//		self.showWaitOverlay()
        let size = CGSize(width: 30, height: 30)
        let indicatorType = NVActivityIndicatorType.ballPulseSync
        startAnimating(size, message: "Loading...", type: indicatorType, fadeInAnimation: nil)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print(error!)
//				self.removeAllOverlays()
				DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
					self.stopAnimating()
				}
                return
            }
            guard let data = data else {
                print("Data is empty")
//				self.removeAllOverlays()
				DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
					self.stopAnimating()
				}
                return
            }
            
            let json = try! JSON(data: data)

			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.4) {
				self.stopAnimating()
			}
			DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
				NVActivityIndicatorPresenter.sharedInstance.setMessage("Done")
			}
//            self.removeAllOverlays()
            //			print(json)
			let paramUrlEncode = URL(string: paramUrl.absoluteString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            for (_, subJson):(String, JSON) in json["parseLink"] {
                self.parseUrlArray.append(subJson.string!)
                self.getPreview(urlParam: subJson.string!, urlOrigin: paramUrlEncode!)
                //				print(subJson)
            }
        }
        
        task.resume()
        
    }
    

//	.absoluteString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

    
    
    func getPreview(urlParam: String, urlOrigin: URL) {
        //        let url = URL(string: "https://httpbin.org/ip")
        let parseUrl = "http://ec2-54-88-114-129.compute-1.amazonaws.com:9000/oembed?url="
        
        var url = URL(string: "")
        
        if urlParam.lowercased().range(of:"http") != nil {
            print("exists")
            url = URL(string: parseUrl + urlParam)
        } else {
            print("none")
            let originUrl = (urlOrigin.scheme)! + "://" + (urlOrigin.host)!
            url = URL(string: parseUrl + originUrl + urlParam)
        }

		guard url != nil else {
			return
		}

		url = URL(string: (url?.absoluteString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!)
//		searchBarOutlet.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)

//        print((url?.scheme)! + "://" + (url?.host)!)
        print(url!)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let jsonTest = try! JSON(data: data)

            if let title = jsonTest["title"].string {
                if var thumbnail_url = jsonTest["thumbnail_url"].string {
                    if thumbnail_url.lowercased().range(of:"http") != nil || thumbnail_url.lowercased().range(of:"https") != nil {
                        if urlParam.lowercased().range(of:"http") != nil || urlParam.lowercased().range(of:"https") != nil {
                            url = URL(string: urlParam)
                        } else {
                            let originUrl = (urlOrigin.scheme)! + "://" + (urlOrigin.host)!
                            url = URL(string: originUrl + urlParam)
                        }
                    } else {
						//#bug, 여기 풀면 한글이 문제가 생김
//						let originUrl = (urlOrigin.scheme)! + "://" + (urlOrigin.host)!
//						thumbnail_url = originUrl + thumbnail_url
						//#bug, 여기 풀면 한글이 문제가 생김

//						print(#line, (urlOrigin.scheme)! + "://" + (urlOrigin.host)!)
//						print(#line, urlOrigin.host)
//						let originUrl = (URL(string: urlOrigin.absoluteString.removingPercentEncoding!)?.scheme)! + "://" + (URL(string: urlOrigin.absoluteString.removingPercentEncoding!)?.host)!
//                        thumbnail_url = originUrl + thumbnail_url
                    }

                    let data = try? Data(contentsOf: URL(string: thumbnail_url)!)

                    if let imageData = data {
                        let image = UIImage(data: imageData)

                        if let description = jsonTest["description"].string {
                            print(description)
                            if urlParam.lowercased().range(of:"http") != nil {
                                print("exists")
                                url = URL(string: urlParam)
                            } else {
                                print("none")
                                let originUrl = (urlOrigin.scheme)! + "://" + (urlOrigin.host)!
                                url = URL(string: originUrl + urlParam)
                            }
                            self.cellArray.append([image!, title, description, url!])
                        } else {
                            self.cellArray.append([image!, title, "description", url!])
                        }
                    }
                    print(thumbnail_url)
                } else {
                    let dummyText = "dummy"
                    if let description = jsonTest["description"].string {
                        self.cellArray.append([dummyText, title, description, url!])
                    } else {
                        //이미지 없고, 디스크립션 없으면 추가하지 않는다
                        //						self.cellArray.append([dummyText, title, "description", url!])
                    }
                }
            }
            
            if error == nil {
                DispatchQueue.main.async() {
                    self.collectionView?.reloadData()
                    print("reload")
                    print(self.cellArray.count)
                }
            }
            
        }
        DispatchQueue.main.async {
            self.download_requests.append(task)
            task.resume()
        }
    }


    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        //stop all download requests
        for request in download_requests
        {
            request.cancel()
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return cellArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

		if (self.cellArray[indexPath.row][0] is String) {
			let noImgLinkCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "NoImgLinkCollectionViewCell", for: indexPath) as! NoImgLinkCollectionViewCell
			noImgLinkCollectionViewCell.titleLael.text = (self.cellArray[indexPath.row][1] as! String)
			noImgLinkCollectionViewCell.contentsLabel.text = (self.cellArray[indexPath.row][2] as! String)
			return noImgLinkCollectionViewCell
		} else {
			let linkCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! LinkCollectionViewCell
			linkCollectionViewCell.parseImage.image = (self.cellArray[indexPath.row][0] as! UIImage)
			linkCollectionViewCell.parseTitle.text = (self.cellArray[indexPath.row][1] as! String)
//			linkCollectionViewCell.parseContent.text = (self.cellArray[indexPath.row][2] as! String)
			return linkCollectionViewCell
		}
    }

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
//		return CGSize(width: 320.0, height: 200.0)
		return CGSize(width: (UIScreen.main.bounds.width/3 - 8), height: 200.0)
	}


    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let urlDataDict:[String: String] = ["url": (self.cellArray[indexPath.row][3] as! URL).absoluteString]
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: urlDataDict)
        }
    }


    @objc func close() {
        print("close")
        self.dismiss(animated: true)
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
