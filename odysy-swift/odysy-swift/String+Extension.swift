//
//  String+Extension.swift
//  odysy-swift
//
//  Created by ted on 7/4/17.
//  Copyright © 2017 Ted Park. All rights reserved.
//


import Foundation
import UIKit

extension String {

	var canOpenURL : Bool {

		guard let url = NSURL(string: self) else {return false}
		if !UIApplication.shared.canOpenURL(url as URL) {return false}
		let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
		let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
		return predicate.evaluate(with: self)
	}


    func isValidForUrl()->Bool{

		if(self.lowercased().hasPrefix("http") || self.lowercased().hasPrefix("https")){
            return true
        }
        return false
    }

}
