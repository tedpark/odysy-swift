//
//  LinkTableViewCell.swift
//  odysy-swift
//
//  Created by Ted Park on 5/26/17.
//  Copyright © 2017 Ted Park. All rights reserved.
//

import UIKit

class LinkTableViewCell: UITableViewCell {

	@IBOutlet weak var parseImage: UIImageView!
	@IBOutlet weak var parseTitle: UILabel!
	@IBOutlet weak var parseContent: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
