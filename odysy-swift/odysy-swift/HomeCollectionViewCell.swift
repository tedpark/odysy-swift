//
//  HomeCollectionViewCell.swift
//  odysy-swift
//
//  Created by Ted on 7/13/17.
//  Copyright © 2017 Ted Park. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
