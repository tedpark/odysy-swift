//
//  MainNavController.swift
//  odysy-swift
//
//  Created by ted on 6/9/17.
//  Copyright © 2017 Ted Park. All rights reserved.
//

import UIKit

class MainNavController: UINavigationController {

	var mainNavPassUrl = String()

    override func viewDidLoad() {
        super.viewDidLoad()

		guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "LinkTableViewController") as? LinkTableViewController else {
			print("Could not instantiate view controller with identifier of type SecondViewController")
			return
		}
//		vc.mainNavPassUrl = webView.url!.absoluteString
		self.navigationController?.pushViewController(vc, animated: true)

//
//		self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LinkTableViewController.close))
//		print(#line, passUrl)
//		print(passUrl)
//		self.getHTML(url: URL(string: passUrl)!
//
//		ss.passUrl = mainNavPassUrl
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
