//
//  NoImgLinkCollectionViewCell.swift
//  odysy-swift
//
//  Created by ted on 8/17/17.
//  Copyright © 2017 Ted Park. All rig  hts reserved.
//

import UIKit

class NoImgLinkCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var titleLael: UILabel!
	@IBOutlet weak var contentsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.layer.borderColor = UIColor.lightGray.cgColor
		self.layer.borderWidth = 0.3
    }

}
