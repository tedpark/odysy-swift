//
//  LinkCollectionViewCell.swift
//  odysy-swift
//
//  Created by ted on 6/23/17.
//  Copyright © 2017 Ted Park. All rights reserved.
//

import UIKit

class LinkCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var parseImage: UIImageView!
    @IBOutlet weak var parseTitle: UILabel!
    @IBOutlet weak var parseContent: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		self.layer.borderColor = UIColor.lightGray.cgColor
		self.layer.borderWidth = 0.4
    }

}
