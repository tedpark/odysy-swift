//
//  HistoryTableViewController.swift
//  odysy-swift
//
//  Created by Ted Park on 08/04/2019.
//  Copyright © 2019 Ted Park. All rights reserved.
//

import UIKit
import RealmSwift


class HistoryTableViewController: UITableViewController {
    let realm = try! Realm()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.close))
        self.navigationItem.rightBarButtonItem?.tintColor = .black

    }

    @objc func close() {
        self.dismiss(animated: true) {
            print("CloseBtn")
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return realm.objects(URLData.self).count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.textLabel?.text = realm.objects(URLData.self).reversed()[indexPath.row].url
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(realm.objects(URLData.self).reversed()[indexPath.row].url)
        let urlDataDict:[String: String] = ["url": realm.objects(URLData.self).reversed()[indexPath.row].url]
        self.dismiss(animated: true) {
            print("CloseBtn")
            NotificationCenter.default.post(name: Notification.Name("goToUrl"), object: nil, userInfo: urlDataDict)
        }
    }



}
