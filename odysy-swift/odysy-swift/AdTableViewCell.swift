//
//  AdTableViewCell.swift
//  odysy-swift
//
//  Created by Ted on 9/5/17.
//  Copyright © 2017 Ted Park. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdTableViewCell: UITableViewCell, GADBannerViewDelegate {

	let appid : String = "ca-app-pub-8354514655539274/3172381224"

    @IBOutlet var adBannerView: GADBannerView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        adBannerView?.adUnitID = appid
        adBannerView?.delegate = self
//        adBannerView?.rootViewController = self

    }

    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
//        tableView.tableHeaderView?.frame = bannerView.frame
//        tableView.tableHeaderView = bannerView

    }

    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("Fail to receive ads")
        print(error)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
