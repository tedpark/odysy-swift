//
//  ViewController.swift
//  odysy-swift
//
//  Created by ted on 1/15/17.
//  Copyright © 2017 Ted Park. All rights reserved.
//

import Font_Awesome_Swift
import RealmSwift
import UIKit
import WebKit

class URLData: Object {
    @objc dynamic var url = ""
}

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate {
    @available(iOS 8.0, *)
    func updateSearchResults(for searchController: UISearchController) {}

    let realm = try! Realm()
    @IBOutlet var searchBarOutlet: UISearchBar!
    @IBOutlet var progreeBarOutlet: UIProgressView!

    var webView: WKWebView!

    @IBOutlet var backBtnOutlet: UIButton!
    @IBOutlet var forwardBtnOutlet: UIButton!
    @IBOutlet var linkBtnOutlet: UIButton!
    @IBOutlet var shareBtnOutlet: UIButton!
    @IBOutlet var tabBtnOutlet: UIButton!

    @IBAction func goBack(_ sender: Any) {
        print(#line, webView.canGoBack)
        if webView.canGoBack {
            webView.goBack()
        }
    }

    @IBAction func goFoward(_ sender: Any) {
        print(#line, webView.canGoForward)
        if webView.canGoForward {
            webView.goForward()
        }
    }

    @IBAction func linkButtonAction(_ sender: Any) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LinkCollection") as? UINavigationController else {
                print("Could not instantiate view controller with identifier of type LinkCollectionViewController")
                return
            }
            present(vc, animated: true) {
                print(#line, self.webView.url!.absoluteString)
                let urlDataDict: [String: String] = ["url": self.webView.url!.absoluteString]
                NotificationCenter.default.post(name: Notification.Name("StartCollectionNotificationIdentifier"), object: nil, userInfo: urlDataDict)
            }
        } else if UIDevice.current.userInterfaceIdiom == .phone {
            guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainNavController") as? MainNavController else {
                print("Could not instantiate view controller with identifier of type SecondViewController")
                return
            }
            present(vc, animated: true) {
                print(#line, self.webView.url!.absoluteString)
                let urlDataDict: [String: String] = ["url": self.webView.url!.absoluteString]
                NotificationCenter.default.post(name: Notification.Name("StartNotificationIdentifier"), object: nil, userInfo: urlDataDict)
            }
        }
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if urlAlreadyExist(kUrlKey: "urlHistory") {
            if UserDefaults.standard.url(forKey: "urlHistory")?.absoluteString == String(describing: navigationAction.request) {
            } else {
//                self.saveDb(urlString: String(describing: navigationAction.request))
            }
        }

        decisionHandler(.allow)
    }

    func saveDb(urlString: String) {
        if urlAlreadyExist(kUrlKey: "urlHistory") {
            print(#line, urlString)
            if urlString.isEqual(UserDefaults.standard.url(forKey: "urlHistory")!.absoluteString) {
                return
            } else {
                print(#line, urlString)
                let urlData = URLData()
                urlData.url = urlString
                try! realm.write {
                    realm.add(urlData)
                }
            }
        }
    }

    @IBAction func TabActionBtn(_ sender: Any) {
        print("TabActionBtn")
        for list in webView.backForwardList.backList {
            print(list.url)
        }

        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HistoryTableViewController") as? UINavigationController else {
            return
        }
        present(vc, animated: true) {}
    }

    @IBAction func ShareBtnAction(_ sender: Any) {
        //		let firstActivityItem = "ODYSY"
        let secondActivityItem: URL = webView.url!

        let activityVC: UIActivityViewController = UIActivityViewController(
            activityItems: [secondActivityItem], applicationActivities: nil)

        activityVC.excludedActivityTypes = []

        activityVC.popoverPresentationController?.sourceView = view
        if #available(iOS 12.0, *) {
            activityVC.popoverPresentationController?.sourceRect = (sender as AnyObject).frame
        } else {
            // Fallback on earlier versions
        }

        present(activityVC, animated: true, completion: nil)
    }

    @objc func methodOfReceivedNotification(notification: Notification) {
        print(notification.userInfo!)
        if let paramUrl = notification.userInfo?["url"] as? String {
            let url = NSURL(string: paramUrl)
            let request = NSURLRequest(url: url! as URL)
            webView.load(request as URLRequest)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        var hasTopNotch: Bool {
            if #available(iOS 11.0, *) {
                return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
            }
            return false
        }

        progreeBarOutlet.isHidden = true
        webView = WKWebView(frame: CGRect(x: 0, y: 64 + (hasTopNotch ? 20 : 0),
                                          width: view.frame.width,
                                          height: view.frame.height - 64 - 44 - (hasTopNotch ? 60 : 0)))
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.allowsBackForwardNavigationGestures = true

        if urlAlreadyExist(kUrlKey: "urlHistory") {
            let request = NSURLRequest(url: UserDefaults.standard.url(forKey: "urlHistory")!)
            webView.load(request as URLRequest)
        } else {
            let url = NSURL(string: "https://medium.com/@xpinn/enjoy-web-space-odyssey-with-odysy-5f5bfed35eff")
            let request = NSURLRequest(url: url! as URL)
            webView.load(request as URLRequest)
        }

        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(saveUserDefault),
                                               name: .UIApplicationDidEnterBackground,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(saveUserDefault),
                                               name: .UIApplicationWillTerminate,
                                               object: nil)

        searchBarOutlet.delegate = self

        let sizeImage = CGFloat(10)

        backBtnOutlet.imageView?.contentMode = .scaleAspectFit
        backBtnOutlet.imageEdgeInsets = UIEdgeInsetsMake(sizeImage, sizeImage, sizeImage, sizeImage)
        backBtnOutlet.setImage(UIImage(named: "chevron-left"), for: UIControlState.normal)

        forwardBtnOutlet.imageView?.contentMode = .scaleAspectFit
        forwardBtnOutlet.imageEdgeInsets = UIEdgeInsetsMake(sizeImage, sizeImage, sizeImage, sizeImage)
        forwardBtnOutlet.setImage(UIImage(named: "chevron-right"), for: UIControlState.normal)

        linkBtnOutlet.imageView?.contentMode = .scaleAspectFit
        linkBtnOutlet.imageEdgeInsets = UIEdgeInsetsMake(sizeImage, sizeImage, sizeImage, sizeImage)
        linkBtnOutlet.setImage(UIImage(named: "tabs-alt"), for: UIControlState.normal)

        tabBtnOutlet.imageView?.contentMode = .scaleAspectFit
        tabBtnOutlet.imageEdgeInsets = UIEdgeInsetsMake(sizeImage, sizeImage, sizeImage, sizeImage)
        tabBtnOutlet.setImage(UIImage(named: "anchor"), for: UIControlState.normal)

        shareBtnOutlet.imageView?.contentMode = .scaleAspectFit
        shareBtnOutlet.imageEdgeInsets = UIEdgeInsetsMake(sizeImage, sizeImage, sizeImage, sizeImage)
        shareBtnOutlet.setImage(UIImage(named: "share"), for: UIControlState.normal)

        NotificationCenter.default.addObserver(self, selector: #selector(methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(goToUrl(notification:)), name: Notification.Name("goToUrl"), object: nil)

        view.addSubview(webView)
        view.sendSubview(toBack: webView)
    }

    deinit {
        print("Remove NotificationCenter Deinit")
        NotificationCenter.default.removeObserver(self)
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            webView.frame = CGRect(x: 0, y: 64, width: view.frame.height, height: view.frame.width - 64)
        } else {
            print("Portrait")
            webView.frame = CGRect(x: 0, y: 64, width: view.frame.height, height: view.frame.width - 64)
        }
    }

    @objc func goToUrl(notification: Notification) {
        print(notification.userInfo!["url"]!)
        webView.load(URLRequest(url: URL(string: notification.userInfo!["url"]! as! String)!))
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "testSegue" {
            print("testSegue")
            print(webView.url!)
            print(webView.url!.absoluteString)
            if let vc: LinkTableViewController = segue.destination as? LinkTableViewController {
                vc.passUrl = webView.url!.absoluteString
            }
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progreeBarOutlet.progress = Float(webView.estimatedProgress)
            progreeBarOutlet.isHidden = webView.estimatedProgress == 1
        }
    }

    func urlAlreadyExist(kUrlKey: String) -> Bool {
        return UserDefaults.standard.url(forKey: kUrlKey) != nil
    }

    @objc func saveUserDefault() {
        print("save user default")
        UserDefaults.standard.set(webView.url!, forKey: "urlHistory")
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBarOutlet.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!.count <= 0 {
            print("if count 0")
            return
        }

        if URL(string: searchBarOutlet.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!) == nil {
            print("if nil")
            return
        }

        var parsingUrl: URL = URL(string: searchBarOutlet.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)!

        let text = searchBarOutlet.text!
        let types: NSTextCheckingResult.CheckingType = .link

        let detector = try? NSDataDetector(types: types.rawValue)

        let matches = detector?.matches(in: text, options: .reportCompletion, range: NSMakeRange(0, text.count))

        for match in matches! {
            print(#line, match.url!)
            parsingUrl = match.url!
        }

        if parsingUrl.absoluteString.isValidForUrl() {
            let url = parsingUrl
            let request = URLRequest(url: url as URL)
            DispatchQueue.main.async {
                self.webView!.load(request as URLRequest)
            }
        } else {
            let url = URL(string: "https://www.google.com/#q=" + searchBarOutlet.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)
            let request = URLRequest(url: url! as URL)
            DispatchQueue.main.async {
                self.webView!.load(request as URLRequest)
            }
        }

        searchBarOutlet.endEditing(true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }

    // MARK: - WKNavigationDelegate

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
        print("fail to load")
        //		self.removeAllOverlays()
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
        saveDb(urlString: webView.url!.absoluteString)
        searchBarOutlet.text = webView.url?.absoluteString
        progreeBarOutlet.isHidden = false
        saveUserDefault()
        //		self.showWaitOverlay()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        //		self.removeAllOverlays()
        backBtnOutlet.isEnabled = webView.canGoBack
        forwardBtnOutlet.isEnabled = webView.canGoForward
        navigationItem.title = webView.title
        saveUserDefault()
    }

    // this handles target=_blank links by opening them in the same view
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        return nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension UIViewController {
    func presentOnRoot(with viewController: UIViewController) {
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        present(navigationController, animated: false, completion: nil)
    }
}
