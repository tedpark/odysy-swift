//
//  NoImgLinkTableViewCell.swift
//  odysy-swift
//
//  Created by ted on 8/17/17.
//  Copyright © 2017 Ted Park. All rights reserved.
//

import UIKit

class NoImgLinkTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var contentsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
